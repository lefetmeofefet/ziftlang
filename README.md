# Zift Lang

### Grammer
literal = number | string <br>
expression = literal | identifier | list | method_invocation <br>
method_invocation = expression expression (example: obj_exp arg_exp)<br>
scope = {expression, expression, ...} <br>
list = [exp1, exp2 + exp3] | exp1, exp2

### Examples
Hello world:
```
print "hello world"
print 'hello world'
print `hello world`
print .hello_world
```

Arithmetic:
```
print (1 + 2 * 3)
// -> 9
```

Closures:
```
f = (a => (a + 1));
print (f 41);
// -> 42

f2 = (a,b => (a + b));
print (f2 2,3);
// -> 5

f2 = ([a,b] => (a + b));
print (f2 [2,3]);
// -> 5
``` 

Scopes:
```
obj = {
    a = 1
};
print (obj .a);
// -> 1
print (obj "a");
// -> 1

obj2 = {
    x = 2;
    y = 3;
    return (x * y)
};
print obj2
// -> 6
```

Closures + Scopes:
```
f_outer = (a => {
    f_inner = (b => (a + b));
    return f_inner;
});
print (f_outer 1 2)
// -> 3
```

If:
```
a = true;
if a, ([] => {
    print "happened"
}
// -> "happened"
```


# Garbage Fire
a = 2
foo a []
foo a []
foo b []

a = 2
print a
-> 2
obj.a
-> Shit.

(@a)

expression = literal | identifier | grouping
vector = []
###Index
* 
* `This` parameter and scoping
* Scoped macros
* Base functions: define, access, run

### Examples
Variables declaration:
```
// Int
a = 1

// String
b = "beetle"

// Boolean
c = true

// Array
d = [1, 2, 3]

// Object
e = {
    name: "lefet",
    value: 4
}

// Function
f = (num) => {
    return num * num
}
```

Function calling
```
sixteen = f(4)
```

```
a = {
    a: 2,
    b: 3,
    c: (x, y) => {return x + y}
}

a.a = a.c(a.a, a.b)
```

```
a = [1, 2, 3]
b = (param) {return param}
a.add(
    b(a[0])
)
print(a[0] * a[1])
```
a + b
a.add(b)
a + b * c
a.add(b.mult(c))
a * b + C
a.mult(b).add(c)

a = {}
a.b = 3

define(a, {})

define(a.b, 3)
a.define(b, 3)
run(access(access(a, define), execute))


access(a, define) |> access(_, execute) |> run(_, [a,b,c])

### Basic Objects
```javascript
number = token => {
    return {
        value: token,
        add: x => {
            this.value = __add(this.value, x.value)
        },
        ...
    }
}

string = token => {
    return {
        value: token,
        split: add => {
            this.value = __add
        }
    }
}
```


```
(this define a 2)
(this define b {})
(b define a {})
(b.a define {})
(b.a define 2)

(a define b {
    (this define c 2)
    (this define d "c")
})
(this define result (a b))


a = {}
a.a = 2

```

## Functions
Usage: 
```
print "a"
1 + (2 * 3)
owner func [param1 param2]
owner func param1,param2
```
Declaration: 
```
func = ([a b] => a + b)
// Same as:
func = (a,b => a + b)
func 1,2 
-> 3

```
Unevaluated (Symbol) Arguments:
```
func = ([__a, b] => a + b)
func a,"b"
-> "ab"
```

## Scopes
declaration:
```
s = {}
s = {
    x = 2;
    y = 3;
}
x = s.x;
```

## Built In Functions
* Access: every object has access (.) function which returns mapped 
value. If mapping doesnt exist, it returns the `NotFound` object:
```
a = {}
a.b
-> NotFound ["a" "b" env[0]]
```
The `NotFound` object has equals function (`=`) that assigns value to 
env.[0]

#### Build in functions: code
```
. = ([prop] => {

})
```

## TODO
* Function overloading
* Import files
* 

