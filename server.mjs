import express from "express";
// This import makes async errors be caught instead of passing silently
import 'express-async-errors';

let app = express();

// Express JSON parsers
app.use(express.json());

// Serve frontend
app.use("/", express.static("frontend"));


// Create server
let server = app.listen(process.env.PORT || 8008, () => {
    let {port, address} = server.address();
    if (address === "::") {
        address = "http://localhost"
    }
    console.log(`Urizift is UP! ${address}:${port}`)
});
