import {Token, TokenTypes} from "./tokenizer.js";
// TODO: CONCEPT: maybe scope and list are the same thing...? they're both a list of expressions, basically. damn.
//  but list doesnt have an env and mapping, only a list of expressions with no mapping / memory

// TODO: Comment and document code

// TODO: Add code comments with // and /*

// TODO: Add cool string formatting like jarvascripft, and also evaluate the code only later like i want to

/**
 * TODO: Decide if string and number are unique objects in the ast (stringLiteral and numbLiteral) or if they're in the stdlib
 * Unique objects:
 *      - no need for SysCalls, they're just implemented in the runtime object of String / Number
 *      -
 * Stdlib implementation:
 *      - __string and __number global functions
 *      - less runtime logic -> easier to write more runtimes. is it though? more SysCalls to implement... hrmhrm....
 *      - typeAnnotations can be written in stdlib instead of static type checker having to consider the String / Number
 *      -
 * hard.
 */


const ExpressionTypes = {
    SCOPE: "scope",
    INVOCATION: "invocation",
    IDENTIFIER: "identifier",
    LIST: "list",
    LITERAL: "literal"
};


/**
 *
 * @param {Token | [Token]} token
 * @private
 */
function _extractExpression(token) {
    if (token instanceof Array) {
        if (token[0].value === "{") {
            return new Scope(token)
        } else if (token[0].value === "[") {
            return new List(token)
        } else {
            let tokens = _removeParentheses(token);

            if (tokens.length === 0) {
                throw "Expression can't be empty"
            }

            tokens = [...tokens].reverse();
            if (tokens.length === 1) {
                return _extractExpression(tokens[0]);
            } else if (tokens.length === 2) {
                return new Invocation(tokens[1], tokens[0]);
            } else {
                return new Invocation(
                    tokens.slice(1).reverse(),
                    tokens[0],
                );
            }
        }
    } else {
        if (token.tokenType === TokenTypes.STRING) {
            return new Invocation(
                new Token(TokenTypes.IDENTIFIER, "__string"),
                new Literal(token)
            );
        } else if (token.tokenType === TokenTypes.NUMBER) {
            return new Invocation(
                new Token(TokenTypes.IDENTIFIER, "__number"),
                new Literal(token)
            );
        } else if (token.tokenType === TokenTypes.IDENTIFIER ||
            token.tokenType === TokenTypes.SYMBOL) {
            return new Identifier(token)
        } else {
            throw `Got token of unidentified type: ${token.tokenType}`
        }
    }
}

/**
 * @param {[Token | [Token]]} tokens
 * @returns {[Token | Token[]]}
 * @private
 */
function _removeParentheses(tokens) {
    while (true) {
        while (tokens.length > 0 && tokens[0].value === "(") {
            tokens.pop();
            tokens.shift();
        }

        if (tokens.length === 1 && tokens[0].value === "(") {
            tokens = tokens[0]
        } else {
            break
        }
    }
    return tokens
}

class Scope {
    /** @param {[Token | [Token]]} tokens */
    constructor(tokens) {
        // Removes the {}
        tokens.pop();
        tokens.shift();

        /** @type {[Invocation]} */
        this.expressions = [];

        let tokensInExpression = [];
        for (let token of tokens) {
            if (token.value === ";") {
                if (tokensInExpression.length > 0) {
                    this.expressions.push(_extractExpression(tokensInExpression));
                    tokensInExpression = []
                }
            } else {
                tokensInExpression.push(token)
            }
        }
        if (tokensInExpression.length > 0) {
            this.expressions.push(_extractExpression(tokensInExpression));
        }
    }

    json() {
        return {
            type: ExpressionTypes.SCOPE,
            expressions: this.expressions.map(exp => exp.json())
        }
    }
}

class Invocation {
    /**
     *
     * @param {null | Token | [Token]} obj
     * @param {Token | [Token] | Literal} arg
     */
    constructor(obj, arg) {
        /** @type {null | Literal | Identifier | List | Scope | Invocation} */
        this.obj = obj && _extractExpression(obj);

        /** @type {Token | Identifier | List | Scope | Invocation} */
        this.arg = arg instanceof Literal ? arg : _extractExpression(arg);
    }

    json() {
        return {
            type: ExpressionTypes.INVOCATION,
            obj: this.obj.json(),
            arg: this.arg.json()
        }
    }
}

class List {
    constructor(tokens) {
        // Removes the []
        tokens.pop();
        tokens.shift();
        /** @type {[Literal | Identifier | List | Scope | Invocation]} */
        this.items = tokens
            .filter((t, index) => index % 2 === 0) // Remove commas
            .map(t => _extractExpression(t));
    }

    json() {
        return {
            type: ExpressionTypes.LIST,
            items: this.items.map(item => item.json())
        }
    }
}

class Literal {
    constructor(token) {
        this.token = token
    }

    getValue() {
        let value = this.token.value;
        if (value.startsWith("\"") || value.startsWith("'") || value.startsWith("`")) {
            value = value.substring(1, value.length - 1)
        } else if (value.startsWith(".")) {
            value = value.substring(1)
        }
        return value
    }

    json() {
        return {
            type: ExpressionTypes.LITERAL,
            // TODO: Rename value to "literal"
            value: this.getValue()
        }
    }
}

class Identifier {
    /** @param {Token} token */
    constructor(token) {
        this.token = token
    }

    json() {
        return {
            type: ExpressionTypes.IDENTIFIER,
            // TODO: rename value to "identifier"
            value: this.token.value
        }
    }
}

/** @param {[Token | [Token]]} tokens */
function parse(tokens) {
    return new Scope(tokens);
}


export {parse, ExpressionTypes}