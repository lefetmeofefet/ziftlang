import {Keywords, SystemCalls} from "./stdlib.js";
import {ExpressionTypes} from "./parser.js";

// TODO: Maek better error messages. they suck.

// TODO: List and Env are the same. list just doesn't map values. maybe merge them?

// TODO: implement list functionality (add, remove, length, map, filter)

// TODO: Currently, literals (strings and numbers) are distinct by having the property literal (obj.mapping.literal). maybe let anyone
//  be a literal if they want, if they implement __getLiteral? because right now the existence of literal can already be overriden
//  accidentally. lets make it explicit? but how can they make meaningful literals when they can't make them have "value" property?
//  they'll just have mapping property like all other objects. think more, think more.

// TODO: implement assigning new objects on mappings from outside the env
function callSystem(funcName) {
    if (funcName instanceof NonFound) {
        funcName = funcName.nonFoundParam
    }

    let f = {
        [SystemCalls.IF]: /**List*/ifStatement => {
            console.log("CALLED IF", ifStatement);
            /** @type {{type: String, value: String}} */
            let condition = ifStatement.listItems[0];
            /** @type {Env} */
            let env = ifStatement.listItems[1];
            if (condition.value === "1") {
                return env.call(new List(null, []))
            }
            return new Env([], ifStatement.currentEnv, {})
        },
        [SystemCalls.STRING_ADD]: /**List*/args => {
            console.log("CALLED STRING PLUX", args);
            return {
                type: ExpressionTypes.LITERAL,
                value: args.listItems[0].value + args.listItems[1].value
            }
        },
        [SystemCalls.ADD]: /**List*/args => {
            console.log("CALLED NUMBER PLUX", args);
            return {
                type: ExpressionTypes.LITERAL,
                value: (parseFloat(args.listItems[0].value) + parseFloat(args.listItems[1].value)).toString()
            }
        },
        [SystemCalls.SUBTRACT]: /**List*/args => {
            console.log("CALLED SUBTRACT", args);
            return {
                type: ExpressionTypes.LITERAL,
                value: (parseFloat(args.listItems[0].value) - parseFloat(args.listItems[1].value)).toString()
            }
        },
        [SystemCalls.PRINT]: args => {
            console.log("OUTPUT: ", args.value);
            OutputCb(args.value);
        }
    }[funcName];
    return {
        call: args => {
            console.log(`Executing bootstrapped function: ${funcName} with args: `, args);
            return f(args)
        }
    };
}

class Env {
    static CreateBaseEnv() {
        return new Env([], null, {
            "=": {
                __type: "define_var",
                call: () => {
                    throw "Can't call equals (=) symbol"
                }
            },
            "=>": {
                __type: "define_func",
                call: () => {
                    throw "Can't call arrow (=>) symbol"
                }
            },
            "print": {
                __type: "printy printy",
                call: object => {
                    console.log("Printing ", object);
                    let output = object.mapping.literal.value;
                    console.log("OUTPUT", output);
                    OutputCb(output);
                }
            },
            "return": {
                call: returnValue => {
                    return new ReturnValue(returnValue)
                }
            },
            [Keywords.SYSTEM]: {
                call: arg => callSystem(arg)
            }
        })
    }

    constructor(expressions, parentEnv, mapping = {}, isLambda = false) {
        this.parentEnv = parentEnv;
        this.mapping = mapping;
        this.returnValue = null;

        for (let expression of expressions) {
            let output = this.executeExpression(expression);
            if (output instanceof ReturnValue) {
                this.returnValue = output.value;
                break;
            }
            if (isLambda) {
                this.returnValue = output;
                break;
            }
        }
    }

    /**
     *
     * @param expression
     * @param {Env} injectedEnv
     * @returns {*}
     */
    executeExpression(expression, injectedEnv = {}) {
        let expressionHandlers = {
            [ExpressionTypes.SCOPE]: () => {
                let newEnv = new Env(expression.expressions, this, injectedEnv);
                return newEnv.returnValue || newEnv
            },
            [ExpressionTypes.IDENTIFIER]: () => this.findParam(expression.value),
            [ExpressionTypes.LITERAL]: () => expression,
            [ExpressionTypes.INVOCATION]: () => this._executeInvocation(expression, injectedEnv),
            [ExpressionTypes.LIST]: () => new List(this, expression.items),
        };
        return expressionHandlers[expression.type]()
    }

    _executeInvocation(expression, injectedEnv = {}) {
        if (Object.keys(injectedEnv).length > 0) {
            // If we have injected env, create new env from it and run expression inside it
            return new Env([expression], this, injectedEnv, true).returnValue
        }

        let obj = this.executeExpression(expression.obj);
        if (obj.__type === "function_definition") {
            // Don't execute the arguments yet
            return obj.call(expression.arg)
        }
        return obj.call(this.executeExpression(expression.arg))
    }

    call(requestedParam) {
        if (requestedParam instanceof NonFound) {
            // TODO: define it on the env.
            throw `Env called with NonFound parameter: ${requestedParam.nonFoundParam}, not supported yet`
        }

        // Checks if it's a string
        if (!(requestedParam instanceof Env) || requestedParam.mapping.literal == null) {
            throw `Env called with an object which is not a string: ${requestedParam}`
        }

        // Assume we get a __string object
        let requestedParamName = requestedParam.mapping.literal.value;
        let value = this.mapping[requestedParamName];
        if (value == null) {
            throw `Accessed non existing member ${requestedParamName}`
        }
        return value
    }

    /** @param {string} requestedParam */
    findParam(requestedParam) {
        let currentEnv = this;
        while (currentEnv != null) {
            if (currentEnv.mapping.hasOwnProperty(requestedParam)) {
                return currentEnv.mapping[requestedParam]
            }
            currentEnv = currentEnv.parentEnv;
        }

        return new NonFound(this, requestedParam)
    }

    defineVar(name, value) {
        this.mapping[name] = value
    }
}

class ReturnValue {
    constructor(value) {
        this.value = value
    }
}

class NonFound {
    /**
     * @param {Env} currentEnv
     * @param {string} nonFoundParam
     */
    constructor(currentEnv, nonFoundParam) {
        this.currentEnv = currentEnv;
        this.nonFoundParam = nonFoundParam;
    }

    call(arg) {
        if (arg.__type === "define_var") {
            return {
                __type: "equals_function_invocation",
                call: value => {
                    this.currentEnv.defineVar(this.nonFoundParam, value);
                }
            }
        } else if (arg.__type === "define_func") {
            return {
                __type: "function_definition", // This __type prevents the argument from being executed before calling
                call: deferredExpression => {
                    return {
                        call: arg => {
                            let injectedEnv = {};
                            injectedEnv[this.nonFoundParam] = arg;
                            return this.currentEnv.executeExpression(deferredExpression, injectedEnv)
                        }
                    }
                }
            }
        } else {
            throw `Can't call non-found object ${this.nonFoundParam} with anything other than equals, called with ` + arg
        }
    }
}

class List {
    /**
     * @param {Env} currentEnv
     * @param {[Object]} expressions
     */
    constructor(currentEnv, expressions) {
        this.currentEnv = currentEnv;
        this.listItems = expressions.map(ex => this.currentEnv.executeExpression(ex));
    }

    call(firstArg) {
        if (firstArg.__type === "define_func") {
            return {
                __type: "function_definition", // This __type prevents the argument from being executed before calling
                call: deferredExpression => {
                    return {
                        call: args => {
                            // TODO: Allow obj destructuring (unfolding)
                            let injectedEnv = {};

                            if (args instanceof List) {
                                args = args.listItems;
                            } else {
                                args = [args]
                            }

                            let index = 0;
                            while (index < this.listItems.length && index < args.length) {
                                let value = this.listItems[index];
                                let argument = args[index];

                                if (value instanceof NonFound) {
                                    injectedEnv[value.nonFoundParam] = argument;
                                }
                                index += 1
                            }

                            return this.currentEnv.executeExpression(deferredExpression, injectedEnv)
                        }
                    }
                }
            }
        }
    }
}

let OutputCb;

function execute(scope, outputCb = () => null) {
    OutputCb = outputCb;

    let baseEnv = Env.CreateBaseEnv();
    return new Env(scope.expressions, baseEnv, {})
}

export {execute}
