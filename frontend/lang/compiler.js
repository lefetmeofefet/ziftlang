import {createTokens, scopeTokens, formatTokens, Token, TokenTypes} from "./tokenizer.js";
import {parse} from "./parser.js";
import {stdlibCode} from "./stdlib.js";

function compile(code) {
    code = stdlibCode + code;
    let scopedTokens = _getTokens(code);
    scopedTokens = _handleListSugarSyntaxing(scopedTokens);

    console.log("Scope tokens: ", scopedTokens);
    console.log(formatTokens(scopedTokens));

    let tree = parse(scopedTokens);
    console.log(tree);

    return tree.json()
}

function _getTokens(code) {
    let tokensList = createTokens(code);
    tokensList.unshift(new Token(TokenTypes.SCOPING, "{"));
    tokensList.push(new Token(TokenTypes.SCOPING, "}"));
    console.log("Unscoped tokens: ", tokensList);

    return scopeTokens(tokensList);
}

/**
 * Changes tokens to convert list sugar syntax into normal syntax. examples:
 *      - [1+2, 3] => [(1+2), 3]
 *      - 1, 2, 3  => [1, 2, 3]
 * @param {[Token | [Token]]} tokens
 * @returns {[Token | [Token]]}
 * @private
 */
function _handleListSugarSyntaxing(tokens) {
    /**
     * Converts lists with brackets and commas ([1, 2+3, 4]) to single expressions ([(1), (2+3), (4)]). TODO: Don't add unnecessary parenthes
     */
    let handleExplicitList = tokens => {
        let i = 1;
        let newTokens = [tokens[0]];
        let scopedExpression = [new Token(TokenTypes.SCOPING, "(")];

        while (i < tokens.length - 1) {
            let token = tokens[i];
            if (token.value !== ",") {
                scopedExpression.push(token)
            } else {
                scopedExpression.push(new Token(TokenTypes.SCOPING, ")"));
                newTokens.push(scopedExpression);
                scopedExpression = [];

                newTokens.push(token);
                scopedExpression.push(new Token(TokenTypes.SCOPING, "("));
            }
            i += 1;
        }

        if (scopedExpression.length > 1) {
            scopedExpression.push(new Token(TokenTypes.SCOPING, ")"));
            newTokens.push(scopedExpression)
        }

        newTokens.push(tokens[tokens.length - 1]);
        return newTokens
    };

    /**
     * Finds implicit comma separated lists and converts them to lists
     */
    let handlePossibleImplicitList = tokens => {
        let i = 0;
        let insideList = false;
        let isLastTokenComma = false;
        let newTokens = [];
        let listTokens = [];

        while (i < tokens.length) {
            let token = tokens[i];

            if (!insideList) {
                if (token.value !== ",") {
                    newTokens.push(token);
                } else {
                    if (newTokens[newTokens.length - 2].value !== "[") {
                        listTokens.push(new Token(TokenTypes.SCOPING, "["));
                        listTokens.push(newTokens.pop());
                        listTokens.push(token);
                        insideList = true;
                        isLastTokenComma = true;
                    } else {
                        newTokens.push(token);
                    }
                }
            } else {
                if (isLastTokenComma) {
                    if (token.value === ",") {
                        throw "List must have expressions between commas, you wrote two commas in a row"
                    }
                    listTokens.push(token);
                    isLastTokenComma = false
                } else if (token.value === ",") {
                    listTokens.push(token);
                    isLastTokenComma = true;
                } else {
                    listTokens.push(new Token(TokenTypes.SCOPING, "]"));
                    newTokens.push(listTokens);
                    listTokens = [];

                    newTokens.push(token);
                    insideList = false
                }
            }

            i += 1;
        }

        if (insideList) {
            throw "List can't end with a comma (,)"
        }

        return newTokens
    };

    return _tokenMap(tokens, tokens => {
        if (tokens[0].value === "[") {
            return handleExplicitList(tokens);
        }
        return handlePossibleImplicitList(tokens);
    });
}

/**
 * Recursively runs `cb` on lists of tokens, and replaces the tokens with it's output. It's recursive because a token
 * which is an Array will call `cb` with the array's items.
 * @param tokens
 * @param cb
 * @returns {*}
 * @private
 */
function _tokenMap(tokens, cb) {
    let outputTokens = tokens.map(token => {
        if (token instanceof Array) {
            return _tokenMap(token, cb);
        }
        return token
    });

    return cb(outputTokens)
}

export {compile}
