// TODO: add line numbers to symbols for debugging and errors and whartnort

const TokenTypes = {
    IDENTIFIER: "IDENTIFIER",
    STRING: "STRING",
    NUMBER: "NUMBER",
    SCOPING: "SCOPING",
    SYMBOL: "SYMBOL"
};


class Token {
    static get TokenSelectors() {
        return {
            [TokenTypes.IDENTIFIER]: /^[a-z|A-Z_][a-z|A-Z_0-9]*/,
            [TokenTypes.STRING]: /^(('(\\.|[^'])*')|("(\\.|[^"])*")|(`(\\.|[^`])*`)|(\.[a-z|A-Z_]*))/,
            [TokenTypes.NUMBER]: /^\d+/,
            [TokenTypes.SCOPING]: /^([\[\](){}])/,
            [TokenTypes.SYMBOL]: /^(@|=>|>=|<=|==|\+=|-=|\*=|\/=|\+|-|\*|\/|=|>|<|&&|\||%|!|\^|#|\$|~|\?|,|&|;|:|\\)/,
        }
    };

    constructor(tokenType, value) {
        this.tokenType = tokenType;
        this.value = value;
    }
}

function createTokens(code) {
    let tokens = [];
    let stripSpaces = str => str.replace(/^(?:\s|\t|\n|\r|\r\n)+/, "");

    while (true) {
        code = stripSpaces(code);
        if (code === "") {
            return tokens
        }

        let tokenMatched = false;
        for (let tokenType in TokenTypes) {
            let regexResult = Token.TokenSelectors[tokenType].exec(code);
            if (regexResult) {
                let tokenValue = regexResult[0];
                tokens.push(new Token(tokenType, tokenValue));
                code = code.substring(tokenValue.length);
                tokenMatched = true;
                break
            }
        }

        if (!tokenMatched) {
            throw `No tokens match the remaining code: ${code}`
        }
    }
}

/**
 * @param {[Token]} tokens
 */
function scopeTokens(tokens) {
    const OPENING_TOKENS = ["(", "[", "{"];
    const CLOSING_TOKENS = {
        ["("]: ")",
        ["["]: "]",
        ["{"]: "}"
    };

    function scopeTokensRecursive(tokens, index, closingToken) {
        let tokensInScope = [tokens[index]];
        index += 1;
        while (true) {
            if (index === tokens.length) {
                throw `Reached end of code and scope was never closed (expected ${closingToken})`
            }
            let token = tokens[index];

            if (OPENING_TOKENS.includes(token.value)) {
                let expectedClosingToken = CLOSING_TOKENS[token.value];
                let {_tokens, _index} = scopeTokensRecursive(tokens, index, expectedClosingToken);
                index = _index;
                tokensInScope.push(_tokens);
            } else {
                tokensInScope.push(token);
                index += 1;
                if (token.value === closingToken) {
                    return {
                        _tokens: tokensInScope,
                        _index: index
                    }
                }
                if (CLOSING_TOKENS.hasOwnProperty(token.value)) {
                    throw `Scope was never closed (expected ${closingToken}, got ${token.value})`
                }
            }
        }
    }

    let {_tokens, _index} = scopeTokensRecursive(tokens, 0, "}");
    return _tokens;
}

/**
 *
 * @param {[Token]} tokens
 * @param {string} prefix
 * @returns {string}
 */
function formatTokens(tokens, prefix = "") {
    let str = tokens[0].value;
    prefix += "  ";
    str += "\n" + prefix;

    let finishedScope = false;
    for (let token of tokens.slice(1, -1)) {
        if (token instanceof Array) {
            str += formatTokens(token, prefix);
            finishedScope = true;
        } else {
            if (finishedScope) {
                str += "\n" + prefix;
            }
            finishedScope = false;
            str += token.value + " "
        }
    }


    prefix = prefix.substring(0, prefix.length - 2);
    str += "\n" + prefix;
    str += tokens[tokens.length - 1].value;
    return str
}

export {createTokens, scopeTokens, formatTokens, Token, TokenTypes}