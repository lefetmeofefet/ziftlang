// TODO: Move more stuff to Keywords (system, env) and System (add, print) and Builtins (true, false, __string) consts.
//  Maybe make 3 const types: SystemCalls, StdObjects, and DefaultEnvObjects

// TODO: add else, for, while, maybe

const SystemCalls = {
    PRINT: "___print",
    ADD: "___add",
    SUBTRACT: "___subtract",
    MULTIPLY: "___multiply",
    STRING_ADD: "___strAdd",
    DIVIDE: "___divide",
    IF: "___if",
};

const Keywords = {
    SYSTEM: "___system",
    ENV: "___env"
};


const stdlibCode = `
__string = (literal => {
    __add = (
        s => (
            __string (${Keywords.SYSTEM} ${SystemCalls.STRING_ADD} [literal, s.literal])
        )
    );
    __print = ([] => {${Keywords.SYSTEM} ${SystemCalls.PRINT} literal});
});

__number = (literal => {
    __operation = (
        sysCall => (
            n => (
                __number (${Keywords.SYSTEM} sysCall [literal, n.literal])
            )
        ) 
    );
    __add = (__operation ${SystemCalls.ADD});
    __subtract = (__operation ${SystemCalls.SUBTRACT});
    __print = ([] => {${Keywords.SYSTEM} ${SystemCalls.PRINT} literal});
});

if = ([condition, callback] => {
    return (${Keywords.SYSTEM} ${SystemCalls.IF} [condition.literal, callback]);
});

true = 1;
false = 0;

+ = "__add";
- = "__subtract";
* = "__multiply";
/ = "__divide";
: = =;
`;


export {stdlibCode, Keywords, SystemCalls}
